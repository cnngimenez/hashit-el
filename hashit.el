;;; hashit.el --- Hash it! implementation  -*- lexical-binding: t; -*-

;; [[file:README.org::*Metadata][Metadata:1]]
;; Copyright 2022 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: convenience
;; URL: https://gitlab.com/cnngimenez/hashit-el
;; Package-Requires: ((emacs "27.1"))
;; Metadata:1 ends here

;; [[file:README.org::*License header][License header:1]]
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;; License header:1 ends here

;; [[file:README.org::*Commentary][Commentary:1]]
;;; Commentary:
;;
;; This is an Elisp implementation of Hash it!, a password hasher.
;;
;; The goal is to generate a password for a specific site and a master
;; password.  The master password is yours, hidden, and kept secret
;; from anyone.  The site is identified by a tag, which it is usually
;; the site domain name.  The site tag can be written in a notebook
;; or any place for you to remember.  If someone see your "tags"
;; notebook, no harm is done: the master password is the most secret
;; important thing, not the tags.
;;
;; Hash it! always generate the same password with the same tag and
;; master password.  So, you do not require to remember all Web site
;; passwords, you can regenerate the same password with the site tag
;; and master password.
;;
;; There are two other implementation of Hash it! One made for Android
;; available on F-droid:
;; - https://f-droid.org/es/packages/com.ginkel.hashit/
;; - https://github.com/ginkel/hashit
;;
;; And one for Firefox:
;; - https://addons.mozilla.org/en-US/firefox/addon/password-hasher-plus/
;; - https://github.com/simu/passwordhasherplus
;; 
;; Both implementation generates the same password with the same tag
;; and master password (possibly a little configuration is required).
;;
;;; Code:
;; Commentary:1 ends here

;; [[file:README.org::*Provide][Provide:1]]
(provide 'hashit)
;; Provide:1 ends here

;; [[file:README.org::*Usual Emacs libraries][Usual Emacs libraries:1]]
(require 'cl-lib)
(require 'seq)
;; Usual Emacs libraries:1 ends here

;; [[file:README.org::*hmac-def][hmac-def:1]]
(require 'hmac-def)
;; hmac-def:1 ends here

;; [[file:README.org::*ert for unit-testing][ert for unit-testing:1]]
(require 'ert)
;; ert for unit-testing:1 ends here

;; [[file:README.org::*hashit--sha1-binary][hashit--sha1-binary:1]]
(defun hashit--sha1-binary (string)
  "Calculate the SHA1 hash from STRING and return a binary string."
  (sha1 string 0 (length string) t))
;; hashit--sha1-binary:1 ends here

;; [[file:README.org::*hmac-sha1][hmac-sha1:1]]
(define-hmac-function hmac-sha1 hashit--sha1-binary 64 20)
;; hmac-sha1:1 ends here

;; [[file:README.org::*hmac-sha1-test][hmac-sha1-test:1]]
(ert-deftest hmac-sha1-test ()
  (should (string-equal
           (hmac-sha1 "tag" "pass")
           "\244\363\276w\207\2557\366\251@\202\366\2645^\322\2030")))
;; hmac-sha1-test:1 ends here

;; [[file:README.org::*hashit--bin-to-b64][hashit--bin-to-b64:1]]
(defun hashit--bin-to-b64 (bin-str)
  "Convert a binary BIN-STR into Base64.
The string BIN-STR is a binary representation, a list of bytes or chars."
  (string-trim (base64-encode-string bin-str) nil "="))
;; hashit--bin-to-b64:1 ends here

;; [[file:README.org::*hashit--bin-to-b64-test][hashit--bin-to-b64-test:1]]
(ert-deftest hashit--bin-to-b64-test ()
  (should (string-equal
           (hashit--bin-to-b64 "\244\363\276w\207\2557\366\251@\202\366\2645^\322\2030")
           "pPMavneHFq039qlAgva0NV7SgzA")))
;; hashit--bin-to-b64-test:1 ends here

;; [[file:README.org::*hashit--hash][hashit--hash:1]]
(defun hashit--hash (tag master-pass)
  "Generate a Base64 hash from TAG and the MASTER-PASS.
The generated string is ready to map it and trim it to a user password."
  (hashit--bin-to-b64 (hmac-sha1 tag master-pass)))
;; hashit--hash:1 ends here

;; [[file:README.org::*hashit--hash-test][hashit--hash-test:1]]
(ert-deftest hashit--hash-test ()
  (should (string-equal (hashit--hash "tag" "pass")
                        "pPMavneHFq039qlAgva0NV7SgzA")))
;; hashit--hash-test:1 ends here

;; [[file:README.org::*hashit--calc-seed][hashit--calc-seed:1]]
(defun hashit--calc-seed (s)
  "Calculate the seed of hash string S.
In Hash it! the seed is the sum of the characters binary code."
  (cl-reduce (lambda (cur res)
               (+ cur res))
             s))
;; hashit--calc-seed:1 ends here

;; [[file:README.org::*hashit--calc-seed-test][hashit--calc-seed-test:1]]
(ert-deftest hashit--calc-seed-test ()
  (should (= (hashit--calc-seed "A")
             65))
  (should (= (hashit--calc-seed "AA")
             130)) ;; (+ 65 65)
  (should (= (hashit--calc-seed "pPMavneHFq039qlAgva0NV7SgzA")
             2350)))
;; hashit--calc-seed-test:1 ends here

;; [[file:README.org::*hashit--char-encode-to-digit][hashit--char-encode-to-digit:1]]
(defun hashit--char-encode-to-digit (c seed)
  "Encode a common char C into a number.

Use SEED to give a little more... randomness?"
  (+ (mod (+ c seed) 10) ?0))
;; hashit--char-encode-to-digit:1 ends here

;; [[file:README.org::*hashit--char-encode-to-digit-test][hashit--char-encode-to-digit-test:1]]
(ert-deftest hashit--char-encode-to-string ()
  (should (= (hashit--char-encode-to-digit ?7 0)
             53)) ;; Character ?5 is 53 in number.
  (should (= (hashit--char-encode-to-digit ?7 2)
             55)))
;; hashit--char-encode-to-digit-test:1 ends here

;; [[file:README.org::*hashit--convert-to-digits][hashit--convert-to-digits:1]]
(defun hashit--convert-to-digits (s seed)
  "Convert a Base64 hashed tag+pass representation into digits.
The S parameter is a Base64 encoded string of the hash.
The SEED parameter is a random number.  In Hash it! is the sum of the
bytes in S."
  (let ((first-digit nil))
    (mapconcat (lambda (c)
                 ;; For some reason, it is not a simple mapping.
                 ;; When there is a sequence of digits together, it
                 ;; uses the first digit to re-encode, skipping the
                 ;; next non-digit character...
                 ;; Is it maybe a mistake from the original program?
                 (format "%c" (if (cl-digit-char-p c)
                                  (if first-digit
                                      c
                                    (setq first-digit c))
                                (let ((temp-next first-digit))
                                  (setq first-digit nil)
                                  (hashit--char-encode-to-digit (or temp-next c) seed)))))
               s "")))
;; hashit--convert-to-digits:1 ends here

;; [[file:README.org::*hashit--inject-character][hashit--inject-character:1]]
(defun hashit--inject-character (sInput offset reserved seed lenOut cStart cNum)
  "Ensure a specific character is in the SINPUT string.
If the character is present, just do nothing, if not return a string
like SINPUT with the character included.
The OFFSET is where to start adding the character.
The RESERVED number of offsets reserved for special characters.
The SEED is a number to provide a little randomness.
The LENOUT is the length of the requested user password.
And CSTART and CNUM states the starting character code and ending (
CSTART + CNUM) codes of special character to ensure its presence.

Basically, this functions ensure that SINPUT have at least one
character from code CSTART to CSTART + CNUM, between the SINPUT
position string OFFSET and LENOUT - RESERVED."
  (let* ((pos0 (mod seed lenOut))
         (pos (mod (+ pos0 offset) lenOut))
         (i 0)
         (already-present nil))

    (while (and (not already-present) (< i (- lenOut reserved)))

      (let* ((i2 (mod (+ pos0 reserved i) lenOut))
             (c (seq-elt sInput i2)))
        (when (and (>= c cStart) (< c (+ cStart cNum)))
          (setq already-present t)))

      (setq i (+ 1 i)))

    (if already-present
        sInput
      (let ((sHead (if (> pos 0)
                       (substring sInput 0 pos)
                     ""))
            (sInject (format "%c" (+ (mod (+ seed
                                             (seq-elt sInput pos))
                                          cNum)
                                     cStart)))
            (sTail (if (< (+ pos 1) (length sInput))
                       (substring sInput (+ pos 1) (length sInput))
                     "")))
        (concat sHead sInject sTail)))))
;; hashit--inject-character:1 ends here

;; [[file:README.org::*hashit--require-digit][hashit--require-digit:1]]
(defun hashit--require-digit (s sum hashWordSize)
  "Ensure a digit is in S string.
Use SUM to add a little randomness.  HASHWORDSIZE is the requested
password length to generate."
  (hashit--inject-character s 0 4 sum hashWordSize 48 10))
;; hashit--require-digit:1 ends here

;; [[file:README.org::*hashit--require-punctuation][hashit--require-punctuation:1]]
(defun hashit--require-punctuation (s sum hashWordSize)
  "Ensure a punctuation is in S string.
Use SUM to add a little randomness.  HASHWORDSIZE is the requested
password length to generate."
  (hashit--inject-character s 1 4 sum hashWordSize 33 15))
;; hashit--require-punctuation:1 ends here

;; [[file:README.org::*hashit--require-mixed-case][hashit--require-mixed-case:1]]
(defun hashit--require-mixed-case (s sum hashWordSize)
  "Ensure a mixed case letter is in S string.
Use SUM to add a little randomness.  HASHWORDSIZE is the requested
password length to generate."
  (hashit--inject-character
   (hashit--inject-character s 2 4 sum hashWordSize 65 26)
   3 4 sum hashWordSize 97 26))
;; hashit--require-mixed-case:1 ends here

;; [[file:README.org::*hashit--convert-to-nonspecial][hashit--convert-to-nonspecial:1]]
(defun hashit--convert-to-nonspecial (c seed)
  "Convert the character C into an alphanumeric charater.
Use SEED to add some randomness."
  (+ (mod (+ c seed) 26) 65))
;; hashit--convert-to-nonspecial:1 ends here

;; [[file:README.org::*hashit--is-alphanum-p][hashit--is-alphanum-p:1]]
(defun hashit--is-alphanum-p (c)
  "Is C an alphanumeric character?
Return t if C is a letter or digit."
  (or (and (<= ?a c) (<= c ?z))
      (and (<= ?A c) (<= c ?Z))
      (and (<= ?0 c) (<= c ?9))))
;; hashit--is-alphanum-p:1 ends here

;; [[file:README.org::*hashit--remove-special-characters][hashit--remove-special-characters:1]]
(defun hashit--remove-special-characters (sInput seed lenOut)
  "Remove any special character from SINPUT.
Special characters are anything not alphanumeric.  If one of this
character is found, replace it with a letter or number.

The SEED parameter provides more random.  The LENOUT is the requested
length of the password."
  (let ((s "")
        (i 0)
        (end-while nil))

    (while (and (not end-while) (< i lenOut))
      (let ((j (string-match "[^a-zA-Z0-9]" (substring sInput i))))
        (if j
            (progn (when (> j 0)
                     (setq s (concat s (substring sInput i (+ i j)))))

                   (setq s (concat s (format "%c" (hashit--convert-to-nonspecial i seed))))
                   (setq i (+ i j 1)))
          (setq end-while t))))

    (when (< i (length sInput))
      (setq s (concat s (substring sInput i))))

    s))
;; hashit--remove-special-characters:1 ends here

;; [[file:README.org::*hashit-generate-password][hashit-generate-password:1]]
(defun hashit-generate-password (site-tag master-key
                                      &optional hash-word-size
                                      require-digit require-punctuation
                                      require-mixed-case
                                      restrict-special restrict-digit)
  "Generate a password like Hash it! does.

This function (as many from this package) has the same parameters as
generateHashWord function from the JS implementation from
passhashcommon.js file in the passwordhasherplus project:
https://github.com/simu/passwordhasherplus

From a SITE-TAG, MASTER-KEY and many other configuration, generate a
password for the given site.  To avoid string length errors, strings
must be unibyte strings! Use `encode-coding-string' to force it, for
example, use this code before:

(encode-coding-string site-tag 'utf-8)

The configuration are as follows:

- HASH-WORD-SIZE is optional, and set as 8 by default.  The length of
  the password to generate.
- REQUIRE-DIGIT is optional, nil by default.  If t, a digit is
  ensured to be present at the generated password.
- REQUIRE-PUNCTUATION is optional, nil by default.  If t, a
  punctuation character is ensured to be present at the generated
  password.
- REQUIRE-MIXED-CASE is optional, nil by default.  If t, a mixed case
  letter is ensured to be present at the generated password.
- RESTRICT-SPECIAL is optional, nil by default.  If t, the generated
  password will not have special characters.
- RESTRICT-DIGIT is optional, nil by default.  If t, the generated
  password will have only digits."
  (let* ((hash-word-size-p (or hash-word-size 8))

         (s (hashit--hash site-tag master-key))
         (sum (hashit--calc-seed s)))

    (substring
     (if restrict-digit
         (hashit--convert-to-digits s sum)
       (progn
         (when require-digit
           (setq s (hashit--require-digit s sum hash-word-size-p)))
         (when (and require-punctuation (not restrict-special))
           (setq s (hashit--require-punctuation s sum hash-word-size-p)))
         (when require-mixed-case
           (setq s (hashit--require-mixed-case s sum hash-word-size-p)))
         (when restrict-special
           (setq s (hashit--remove-special-characters s sum hash-word-size-p)))
         s))
     0 hash-word-size-p)))
;; hashit-generate-password:1 ends here

;; [[file:README.org::*hashit-config-savefile][hashit-config-savefile:1]]
(defcustom hashit-config-savefile "hashit-tags.el"
  "Savefile for all tags stored in hashit."
  :group 'hashit)
;; hashit-config-savefile:1 ends here

;; [[file:README.org::*hashit-config-directory][hashit-config-directory:1]]
(defcustom hashit-config-directory (concat user-emacs-directory "hashit/")
  "Directory where to save the hashit savefile."
  :group 'hashit)
;; hashit-config-directory:1 ends here

;; [[file:README.org::*hashit-savefile][hashit-savefile:1]]
(defun hashit-savefile ()
  "Return the complete path of the savefile."
  (concat hashit-config-directory "/" hashit-config-savefile))
;; hashit-savefile:1 ends here

;; [[file:README.org::*hashit-config][hashit-config:1]]
(cl-defstruct hashit-config
  "Configuration information to generate a hashed password."
  tag length type)
;; hashit-config:1 ends here

;; [[file:README.org::*hashit-config-string-format][hashit-config-string-format:1]]
(defcustom hashit-config-string-format "\"%s\" -> Length: %s; Type: %s."
  "Format used to convert a hashit-config instance into string.
The \"%s\" are replaced by: the tag, the length, and the type respectively.

See `hashit-config-to-string' function.  To customise this further,
redefine/advise the function."
  :group 'hashit)
;; hashit-config-string-format:1 ends here

;; [[file:README.org::*hashit-config-to-string][hashit-config-to-string:1]]
(defun hashit-config-to-string (config)
  "Convert the the hashit-config instance into a string.
The CONFIG parameter is a `hashit-config' instance.
The variable `hashit-config-string-format' is used to format the
string."
  (format hashit-config-string-format
          (hashit-config-tag config)
          (hashit-config-length config)
          (hashit-config-type config)))
;; hashit-config-to-string:1 ends here

;; [[file:README.org::*hashit-config-to-alist][hashit-config-to-alist:1]]
(defun hashit-config-to-alist (config)
  "Convert a hashit-config structure to an alist.
CONFIG is a `hashit-config' structure."
  `((tag . ,(hashit-config-tag config))
    (length . ,(hashit-config-length config))
    (type . ,(hashit-config-type config))))
;; hashit-config-to-alist:1 ends here

;; [[file:README.org::*hashit-alist-to-config][hashit-alist-to-config:1]]
(defun hashit-alist-to-config (alist)
  "Convert a hashit-config structure to an alist.
ALIST is an alist to convert.  Returns a `hashit-config' structure."
  (make-hashit-config :tag (alist-get 'tag alist)
                      :length (alist-get 'length alist)
                      :type (alist-get 'type alist)))
;; hashit-alist-to-config:1 ends here

;; [[file:README.org::*hashit-tags-storage][hashit-tags-storage:1]]
(defvar hashit-tags-storage nil
  "Storage for tags elements.
This variable stores a list of hashit-config instances.
Use `hashit-get-storage' function first to access this variable.")
;; hashit-tags-storage:1 ends here

;; [[file:README.org::*hashit-get-storage][hashit-get-storage:3]]
(defun hashit-get-storage ()
  "Load the `hashit-tags-storage' if needed and return it.
You can call this function first, before modifying `hashit-tags-storage'.  Or use the
the resulting value for reading the variable. See the info manual, section
hashit-get-storage to know about its usage."
  (if hashit-tags-storage
      hashit-tags-storage
    (setq hashit-tags-storage (hashit-tags-load-from-file (hashit-savefile)))))
;; hashit-get-storage:3 ends here

;; [[file:README.org::*hashit-tags-save-to-file][hashit-tags-save-to-file:1]]
(defun hashit-tags-save-to-file (&optional taglist filepath)
  "Save the TAGLIST into a file in FILEPATH.
TAGLIST is a list of hashit-config instances.
If TAGLIST is not provided, use `hashit-tags-storage' variable.
If FILEPATH is not provided, use `hashit-savefile' function."
  (with-temp-buffer
    (insert (prin1-to-string
             (cl-mapcar (lambda (config)
                          (hashit-config-to-alist config))
                        (if taglist taglist hashit-tags-storage))))
    (write-file (if filepath filepath (hashit-savefile)))))
;; hashit-tags-save-to-file:1 ends here

;; [[file:README.org::*hashit-tags-load-from-file][hashit-tags-load-from-file:1]]
(defun hashit-tags-load-from-file (&optional filepath)
  "Load the tag data from a file.
This tag data is a list of alists text saved with `hashit-tags-save-to-file'.

FILEPATH is the file to load.  If not provided, use `hashit-savefile' function."
  (let ((filepathp (if filepath filepath (hashit-savefile))))
    (when (file-exists-p filepathp)
      (with-temp-buffer
        (insert-file-contents filepathp)
        (cl-mapcar (lambda (alist)
                     (hashit-alist-to-config alist))
                   (read (buffer-string)))))))
;; hashit-tags-load-from-file:1 ends here

;; [[file:README.org::*hashit-tags-add][hashit-tags-add:1]]
(defun hashit-tags-add (config)
  "Add a new tag config to the storage.
CONFIG is a `hashit-config' struct."
  (push config (hashit-get-storage)))
;; hashit-tags-add:1 ends here

;; [[file:README.org::*hashit-tags-add-and-save][hashit-tags-add-and-save:1]]
(defun hashit-tags-add-and-save (&optional config)
  "Add a new tag config to the storage.
CONFIG is a `hashit-config' struct.  If not provided then ask the user for a new tag.
Then, the storage is saved to a file."
  (interactive)
  (if config
      (progn
        (hashit-tags-add-or-replace config)
        (hashit-tags-save-to-file))
    ;; It reads the new config and uses this same funtion to store it.
    (hashit-read-and-store-config)))
;; hashit-tags-add-and-save:1 ends here

;; [[file:README.org::*hashit-tags-add-or-replace][hashit-tags-add-or-replace:1]]
(defun hashit-tags-add-or-replace (config)
  "Add a new tag or replace the existing one if found.
CONFIG is a `hashit-config' instance.  If the same tag is found on `hashit-tags-storage',
replace with CONFIG.  If not found, add it."
  (when (hashit-tags-find (hashit-config-tag config))
    (hashit-tags-remove (hashit-config-tag config)))

  (hashit-get-storage)
  (push config hashit-tags-storage))
;; hashit-tags-add-or-replace:1 ends here

;; [[file:README.org::*hashit-tags-add-or-replace-test][hashit-tags-add-or-replace-test:1]]
(ert-deftest hashit-tags-add-or-replace-test ()
  (push (make-hashit-config :tag "test-tag" :length 12 :type 'alphanum)
        hashit-tags-storage)
  (let ((new-tag (make-hashit-config :tag "test-tag"
                                     :length 10
                                     :type 'num)))
    (hashit-tags-add-or-replace new-tag)
    (should (equal new-tag
                   (hashit-tags-find "test-tag")))))
;; hashit-tags-add-or-replace-test:1 ends here

;; [[file:README.org::*hashit-tags-remove][hashit-tags-remove:1]]
(defun hashit-tags-remove (tag)
  "Remove the configuration with TAG.
Search the `hashit-config' instance in `hashit-tags-storage' with the given TAG and
delete it."
  (setq hashit-tags-storage
        (cl-remove-if (lambda (config)
                        (string= (hashit-config-tag config) tag))
                      (hashit-get-storage))))
;; hashit-tags-remove:1 ends here

;; [[file:README.org::*hashit-tags-remove-and-save][hashit-tags-remove-and-save:1]]
(defun hashit-tags-remove-and-save (tag)
  "Remove the configuration with TAG.
Search the `hashit-config' instance in `hashit-tags-storage' with the given TAG and
delete it."
  (interactive (list (completing-read "MTag?"
                                      (mapcar (lambda (config)
                                                (hashit-config-tag config))
                                              (hashit-get-storage))
                                      nil t)))
  (hashit-tags-remove tag)
  (hashit-tags-save-to-file))
;; hashit-tags-remove-and-save:1 ends here

;; [[file:README.org::*hashit-tags-find][hashit-tags-find:1]]
(defun hashit-tags-find (tag)
  "Search for a config by its tag name.
TAG must be a string, the tag name of the config to search in
`hashit-tags-storage'.  The storage is loaded if needed.
Return nil if not found."
  (cl-find-if (lambda (config)
                (string= tag (hashit-config-tag config)))
              (hashit-get-storage)))
;; hashit-tags-find:1 ends here

;; [[file:README.org::*hashit-tags-list][hashit-tags-list:1]]
(defun hashit-tags-list ()
  "List stored tags in a new buffer."
  (interactive)
  (with-current-buffer (get-buffer-create "hahsit-tags")
    (mapcar (lambda (config)
              (insert (hashit-config-to-string config) "\n"))
            (hashit-get-storage))
    (display-buffer (current-buffer))))
;; hashit-tags-list:1 ends here

;; [[file:README.org::*hashit-import-json-to-config][hashit-import-json-to-config:1]]
(defun hashit-import-json-to-config (json-tag)
  "Return a hashit-config instance from the JSON-TAG.
JSON-TAG is a hash-table from an imported JSON data."
  (make-hashit-config :tag (gethash "tag" json-tag)
                      :length (gethash "length" (gethash "policy" json-tag))
                      :type (hashit--custom-hash-to-type
                             (gethash "custom" (gethash "policy" json-tag)))))
;; hashit-import-json-to-config:1 ends here

;; [[file:README.org::*hashit-import-json-to-config-test][hashit-import-json-to-config-test:1]]
(ert-deftest hashit-import-json-to-config-test ()
  (let ((test-hash (json-parse-string "{\"tag\":\"example.org\",\"bump\":0,\"policy\":{\"id\":\"alfanumsym\",\"name\":\"Letras, números y símbolos\",\"seed\":null,\"length\":12,\"strength\":-1,\"custom\":{\"d\":true,\"p\":true,\"m\":true,\"r\":false,\"only_digits\":false}}}")))
    (should (equal (hashit-import-json-to-config test-hash)
                   (make-hashit-config :tag "example.org"
                                       :length 12
                                       :type 'alphanum-special)))))
;; hashit-import-json-to-config-test:1 ends here

;; [[file:README.org::*hashit--type-to-id][hashit--type-to-id:1]]
(defun hashit--type-to-id (type)
  "Return the policy JSON name of the given type."
  (cond ((equal type 'alphanum-special)
         "alfanumsym")
        ((equal type 'alphanum)
         "alfanum")
        ((equal type 'num)
         "pin10")
        (t nil)))
;; hashit--type-to-id:1 ends here

;; [[file:README.org::*hashit--type-to-name][hashit--type-to-name:1]]
(defun hashit--type-to-name (type)
  "Return the policy JSON name of the given type."
  (cond ((equal type 'alphanum-special)
         "Letras, números y símbolos")
        ((equal type 'alphanum)
         "Letras y números")
        ((equal type 'num)
         "PIN de 10 dígitos")
        (t nil)))
;; hashit--type-to-name:1 ends here

;; [[file:README.org::*hashit--type-to-policy][hashit--type-to-policy:1]]
(defun hashit--type-to-policy (length type)
  "Return a hash-table with the policy portion of the JSON export.
LENGTH is the password length.  TYPE is a symbol with the type of
password."
  (let ((result (make-hash-table :test 'equal)))
    (puthash "id" (hashit--type-to-id type) result)
    (puthash "name" (hashit--type-to-name type) result)
    (puthash "seed" nil result)
    (puthash "length" length result)
    (puthash "strength" -1 result)
    (puthash "custom" (hashit--type-to-custom-hash type) result)
    result))
;; hashit--type-to-policy:1 ends here

;; [[file:README.org::*hashit-export-config-to-json][hashit-export-config-to-json:1]]
(defun hashit-export-config-to-json (config)
  "Return a hash-table usable for JSON encoding from a hashit-config.
CONFIG is a `hashit-config' instance to convert to a JSON hash-table
representation.  The return value can be used by `json-encode-hash-table'.
to create a hashit-onsen tag configuration object."
  (let ((result (make-hash-table :test 'equal)))
    (puthash "tag" (hashit-config-tag config) result)
    (puthash "bump" 0 result)
    (puthash "policy" (hashit--type-to-policy (hashit-config-length config)
                                              (hashit-config-type config))
             result)
    result))
;; hashit-export-config-to-json:1 ends here

;; [[file:README.org::*hashit-export-config-to-json-test][hashit-export-config-to-json-test:1]]
(ert-deftest hashit-export-config-to-json-test ()
  (should (string= (json-encode-hash-table
                    (hashit-export-config-to-json
                     (make-hashit-config :tag "example.org"
                                         :length 12
                                         :type 'alphanum-special)))
                   "{\"tag\":\"example.org\",\"bump\":0,\"policy\":{\"id\":\"alfanumsym\",\"name\":\"Letras, números y símbolos\",\"seed\":null,\"length\":12,\"strength\":-1,\"custom\":{\"d\":true,\"p\":true,\"m\":true,\"r\":false,\"only_digits\":false}}}")))
;; hashit-export-config-to-json-test:1 ends here

;; [[file:README.org::*hashit--custom-hash-to-type][hashit--custom-hash-to-type:1]]
(defun hashit--custom-hash-to-type (custom-hash)
  "Convert hashit-onsen custom definition to the hashit.el type."
  (let ((require-digits (gethash "d" custom-hash))
        (require-punctuation (gethash "p" custom-hash))
        (require-mixed-case (gethash "m" custom-hash))
        (only-special-characters (gethash "r" custom-hash))
        (only-digits (gethash "only_digits" custom-hash)))
    (cond
     ((and require-digits require-punctuation require-mixed-case
           (equal :false only-special-characters) (equal :false only-digits))
      'alphanum-special)
     ((and require-digits (equal :false require-punctuation) require-mixed-case
           only-special-characters (equal :false only-digits))
      'alphanum)
     ((and (equal :false require-digits) (equal :false require-punctuation) (equal :false require-mixed-case)
           only-special-characters only-digits)
      'num)
     (t nil))))
;; hashit--custom-hash-to-type:1 ends here

;; [[file:README.org::*hashit--custom-hash-to-type-test][hashit--custom-hash-to-type-test:1]]
(ert-deftest hashit--custom-hash-to-type-test ()
  (let ((test-hash (make-hash-table :test 'equal)))
    ;; Test alphanum-special.
    (puthash "d" t test-hash)
    (puthash "p" t test-hash)
    (puthash "m" t test-hash)
    (puthash "r" :false test-hash)
    (puthash "only_digits" :false test-hash)      
    (should (equal (hashit--custom-hash-to-type test-hash)
                   'alphanum-special))
    ;; Test alphanum.
    (puthash "d" t test-hash)
    (puthash "p" :false test-hash)
    (puthash "m" t test-hash)
    (puthash "r" t test-hash)
    (puthash "only_digits" :false test-hash)
    (should (equal (hashit--custom-hash-to-type test-hash)
                   'alphanum))
    ;; Test num.
    (puthash "d" :false test-hash)
    (puthash "p" :false test-hash)
    (puthash "m" :false test-hash)
    (puthash "r" t test-hash)
    (puthash "only_digits" t test-hash)      
    (should (equal (hashit--custom-hash-to-type test-hash)
                   'num))
    ;; Test others
    (puthash "d" t test-hash)
    (should (equal (hashit--custom-hash-to-type test-hash)
                   nil))))
;; hashit--custom-hash-to-type-test:1 ends here

;; [[file:README.org::*hashit--type-to-custom-hash][hashit--type-to-custom-hash:1]]
(defun hashit--type-to-custom-hash (type)
  "Convert hashit.el type to a hashit-onsen custom definition."
  (let ((new-hash (make-hash-table :test 'equal)))
    (cond ((equal type 'alphanum-special)
           (puthash "d" t new-hash)
           (puthash "p" t new-hash)
           (puthash "m" t new-hash)
           (puthash "r" :json-false new-hash)
           (puthash "only_digits" :json-false new-hash)
           new-hash)
          ((equal type 'alphanum)
           (puthash "d" t new-hash)
           (puthash "p" :json-false new-hash)
           (puthash "m" t new-hash)
           (puthash "r" t new-hash)
           (puthash "only_digits" :json-false new-hash)
           new-hash)
          ((equal type 'num)
           (puthash "d" :json-false new-hash)
           (puthash "p" :json-false new-hash)
           (puthash "m" :json-false new-hash)
           (puthash "r" t new-hash)
           (puthash "only_digits" t new-hash)
           new-hash)
          (t nil))))
;; hashit--type-to-custom-hash:1 ends here

;; [[file:README.org::*hashit--type-to-custom-hash-test][hashit--type-to-custom-hash-test:1]]
(ert-deftest hashit--type-to-custom-hash-test ()
  ;; Test alphanum-special.
  (should (string= (json-encode-hash-table (hashit--type-to-custom-hash 'alphanum-special))
                   "{\"d\":true,\"p\":true,\"m\":true,\"r\":false,\"only_digits\":false}"))
   ;; Test alphanum.
  (should (string= (json-encode-hash-table (hashit--type-to-custom-hash 'alphanum))
                   "{\"d\":true,\"p\":false,\"m\":true,\"r\":true,\"only_digits\":false}"))
  ;; Test num.
  (should (string= (json-encode-hash-table (hashit--type-to-custom-hash 'num))
                   "{\"d\":false,\"p\":false,\"m\":false,\"r\":true,\"only_digits\":true}"))
    ;; Test others
  (should (not (hashit--type-to-custom-hash 'other))))
;; hashit--type-to-custom-hash-test:1 ends here

;; [[file:README.org::*hashit-json-to-all-config][hashit-json-to-all-config:1]]
(defun hashit-json-to-all-config (json)
  "Convert the JSON hash-table with several tags into a list of hashit-config.
JSON is a hash-table obtainde by `json-parse-string' or similar parsing function.
It should contain several tags as exported by hashit-onsen."
  (mapcar #'hashit-import-json-to-config json))
;; hashit-json-to-all-config:1 ends here

;; [[file:README.org::*hashit-json-to-all-config-test][hashit-json-to-all-config-test:1]]
(ert-deftest hashit-json-to-all-config-test ()
  (should (equal (hashit-json-to-all-config
                  (json-parse-string
                   "[{\"tag\":\"example.org\",\"bump\":0,\"policy\":{\"id\":\"alfanumsym\",\"name\":\"Letras, números y símbolos\",\"seed\":null,\"length\":12,\"strength\":-1,\"custom\":{\"d\":true,\"p\":true,\"m\":true,\"r\":false,\"only_digits\":false}}},{\"tag\":\"example.com\",\"bump\":0,\"policy\":{\"id\":\"alfanumsym\",\"name\":\"Letras, números y símbolos\",\"seed\":null,\"length\":12,\"strength\":-1,\"custom\":{\"d\":true,\"p\":true,\"m\":true,\"r\":false,\"only_digits\":false}}}]"))
                 (list (make-hashit-config :tag "example.org" :length 12 :type 'alphanum-special)
                       (make-hashit-config :tag "example.com" :length 12 :type 'alphanum-special)))))
;; hashit-json-to-all-config-test:1 ends here

;; [[file:README.org::*hashit-json-import-buffer][hashit-json-import-buffer:1]]
(defun hashit-json-import-buffer ()
  "Parse the current buffer and import it.
 This is intended for Hashit-onsen implementation.
 See: https://cnngimenez.gitlab.io/hashit-onsen
 This Hash-It implementatione export tags in JSON format."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (mapcar #'hashit-tags-add (hashit-json-to-all-config (json-parse-buffer)))))
;; hashit-json-import-buffer:1 ends here

;; [[file:README.org::*hashit-all-config-to-json][hashit-all-config-to-json:1]]
(defun hashit-all-config-to-json (config-list)
  "Convert all hashit-config instances in the list into JSON hash-table.
CONFIG-LIST is a list with hashit-config instances."
  (map 'array #'hashit-export-config-to-json config-list))
;; hashit-all-config-to-json:1 ends here

;; [[file:README.org::*hashit-all-config-to-json-test][hashit-all-config-to-json-test:1]]
(ert-deftest hashit-all-config-to-json-test ()
  (should (string= (json-encode-hash-table
                    (hashit-all-config-to-json
                     (list (make-hashit-config :tag "example.org" :length 12 :type 'alphanum-special)
                           (make-hashit-config :tag "example.com" :length 12 :type 'alphanum-special))))
                    "[{\"tag\":\"example.org\",\"bump\":0,\"policy\":{\"id\":\"alfanumsym\",\"name\":\"Letras, números y símbolos\",\"seed\":null,\"length\":12,\"strength\":-1,\"custom\":{\"d\":true,\"p\":true,\"m\":true,\"r\":false,\"only_digits\":false}}},{\"tag\":\"example.com\",\"bump\":0,\"policy\":{\"id\":\"alfanumsym\",\"name\":\"Letras, números y símbolos\",\"seed\":null,\"length\":12,\"strength\":-1,\"custom\":{\"d\":true,\"p\":true,\"m\":true,\"r\":false,\"only_digits\":false}}}]")))
;; hashit-all-config-to-json-test:1 ends here

;; [[file:README.org::*hashit-json-export-buffer][hashit-json-export-buffer:1]]
(defun hashit-json-export-buffer ()
  "Export the tag storage into JSON to use with Hashit-onsen."
  (interactive)
  (with-current-buffer (get-buffer-create "hashit-json-export")
    (delete-region (point-min) (point-max))
    (insert (json-encode-hash-table (hashit-all-config-to-json (hashit-get-storage))))
    (display-buffer (current-buffer))))
;; hashit-json-export-buffer:1 ends here

;; [[file:README.org::*hashit-master-key][hashit-master-key:1]]
(defvar hashit-master-key nil
  "Your master key.
Sometimse used as cache for the current session.
It is better to not to save this value into your init.el or
customizations!")
;; hashit-master-key:1 ends here

;; [[file:README.org::*hahsit-read-master-key-maybe][hahsit-read-master-key-maybe:1]]
(defun hashit-read-master-key-maybe ()
  "Read the master key from the user.
If the master key was not provided during this session, ask it."
  (setq hashit-master-key
        (or hashit-master-key (read-passwd "Master key?"))))
;; hahsit-read-master-key-maybe:1 ends here

;; [[file:README.org::*hashit-reset-master-key][hashit-reset-master-key:1]]
(defun hashit-reset-master-key ()
  "Reset the cached master key.
Do not worry, the master key is not permanently stored.  You can find
it at the `hashit-master-key' variable.

This function just clean that variable."
  (interactive)
  (setq hashit-master-key nil))
;; hashit-reset-master-key:1 ends here

;; [[file:README.org::*hashit-password][hashit-password:1]]
(defun hashit-password (site-tag pass-length &optional master-key config)
  "Generate a password compatible with Hash it!
From SITE-TAG and the MASTER-KEY, generate the password for that site.
The length of the generated password is PASS-LENGTH.

A password specification (a.k.a. configuration) is requested if not
provided by CONFIG parameter.  This ensure that certain characters are
used on the password.  There are three possible configurations:
alphanum-special (Alphanumeric + special), alphanum (Alphanumeric),
and num (Numeric).  See `hashit-read-config' and `hashit-current-type' for
more information.

The password is not printed, it will be copied.  Use `yank' to paste
it werever you need.

The MASTER-KEY will be requested once per Emacs session.  If you need
to reset it use `hashit-reset-master-key'."
  (interactive (let ((new-config (hashit-ask-tag)))
                 (list (hashit-config-tag new-config)
                       (hashit-config-length new-config)
                       nil
                       (hashit-config-type new-config))))

  (setq master-key (encode-coding-string
                    (or master-key (hashit-read-master-key-maybe))
                    'utf-8))
  (setq site-tag (encode-coding-string site-tag 'utf-8))

  (let ((configp (hashit-read-config config)))
    (kill-new
     (cond
      ((eq configp 'alphanum-special)
       (hashit-generate-password site-tag master-key pass-length
                                 t t t nil nil))
      ((eq configp 'alphanum)
       (hashit-generate-password site-tag master-key pass-length
                                 t nil t t nil))
      ((eq configp 'num)
       (hashit-generate-password site-tag master-key pass-length
                                 t nil nil nil t)))))
  (message "Password has been copied and its ready for pasting."))
;; hashit-password:1 ends here

;; [[file:README.org::*hashit-current-type][hashit-current-type:1]]
(defvar hashit-current-type 'alphanum-special
  "Which configuration the user used lastly?
Store the last configuration used.

There are three types: alphanum-special, alphanum, and num.")
;; hashit-current-type:1 ends here

;; [[file:README.org::*hashit-read-config][hashit-read-config:1]]
(defun hashit-read-config (&optional config)
  "Return the user configuration for new passwords.
If CONFIG is nil, ask the user which configuration wants.  Else
return CONFIG symbol.  It should be one of the three possible valueso
supported by `hashit-current-type' variable.

In any case, `hashit-current-type' value will be updated and set to this
function output."
  (interactive)
  (setq hashit-current-type
        (if config
            config
          (let ((user-config (completing-read
                              "Configuration?"
                              '("Alphanumeric + special"
                                "Alphanumeric"
                                "Numeric")
                              nil t nil nil "Alphanumeric + special")))
            (cond
             ((string-equal user-config "Alphanumeric + special")
              'alphanum-special)
             ((string-equal user-config "Alphanumeric")
              'alphanum)
             ((string-equal user-config "Numeric")
              'num))))))
;; hashit-read-config:1 ends here

;; [[file:README.org::*hashit-read-and-store-config][hashit-read-and-store-config:1]]
(defun hashit-read-and-store-config (&optional tag)
  "Ask the user about config data and save it for future use.
Save it to `hashit-tags-storage'."
  (let ((new-config
         (make-hashit-config :tag (if tag
                                      tag
                                    (read-from-minibuffer "Tag?"))
                             :length (string-to-number (read-from-minibuffer "Password Length?" "12"))
                             :type (hashit-read-config))))
    (hashit-tags-add-and-save new-config)
    new-config))
;; hashit-read-and-store-config:1 ends here

;; [[file:README.org::*hashit-ask-tag][hashit-ask-tag:1]]
(defun hashit-ask-tag ()
  "Ask the user for the tag and return the configuration.
If the tag is not in the storage, ask for more information."
  (interactive)
  (let* ((user-selection 
          (completing-read "Site tag?"
                           (mapcar (lambda (config)
                                     (hashit-config-tag config))
                                   (hashit-get-storage))))
         (config-selected (hashit-tags-find user-selection)))
    (if config-selected
        config-selected
      (hashit-read-and-store-config user-selection))))
;; hashit-ask-tag:1 ends here

;;; hashit.el ends here
